# Regular Sequence Fluctuations

This repository contains code accompanying the articles
* [**Analysis of Summatory Functions of Regular Sequences: Transducer and Pascal's Rhombus**](https://arxiv.org/abs/1802.03266)
by [Clemens Heuberger](http://wwwu.uni-klu.ac.at/cheuberg/),
[Daniel Krenn](http://www.danielkrenn.at/)
and [Helmut Prodinger](http://www.helmut-prodinger.at/),
* [**Esthetic Numbers and Lifting Restrictions on the Analysis of Summatory Functions of Regular Sequences**](https://arxiv.org/abs/1808.00842)
by [Clemens Heuberger](http://wwwu.uni-klu.ac.at/cheuberg/)
and [Daniel Krenn](http://www.danielkrenn.at/),
and
* [**Asymptotic Analysis of Regular Sequences**](https://arxiv.org/abs/1810.13178)
by [Clemens Heuberger](http://wwwu.uni-klu.ac.at/cheuberg/)
and [Daniel Krenn](http://www.danielkrenn.at/).

## Installation

You need to have SageMath installed. Then say
```
sage --pip install git+https://gitlab.com/dakrenn/regular-sequence-fluctuations.git
```

## Run Examples

To run, use
```
sage --python -m regular_sequence_fluctuations.examples.rhombus_plotter
sage --python -m regular_sequence_fluctuations.examples.esthetic_plotter
```

Note that `esthetic_plotter` needs lots of memory and two hours on a reasonably modern machine (as of 2023); one of the reasons is that it is in base 4 and therefore uses high indices.
