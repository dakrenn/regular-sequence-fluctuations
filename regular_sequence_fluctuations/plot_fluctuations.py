# ****************************************************************************
#       Copyright (C) 2019 Clemens Heuberger <clemens.heuberger@aau.at>
#                     2019 Daniel Krenn <dev@danielkrenn.at>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/
# ****************************************************************************

import time

from sage.all import srange
from sage.calculus.transforms.fft import FastFourierTransform
from sage.functions.log import log
from sage.functions.other import ceil, floor
from sage.matrix.constructor import matrix
from sage.matrix.matrix_space import MatrixSpace
from sage.matrix.special import diagonal_matrix, identity_matrix
from sage.misc.cachefunc import cached_method
from sage.misc.misc_c import prod
from sage.modules.free_module import VectorSpace
from sage.rings.all import CC
from sage.rings.complex_arb import ComplexBallField
from sage.rings.infinity import Infinity
from sage.rings.integer_ring import ZZ
from sage.rings.qqbar import QQbar
from sage.rings.real_mpfr import RR
from sage.symbolic.constants import pi


def reduce_resolution(data, x_min, x_max, resolution):
    r"""
    Reduce a list of pairs `(x, y)` to a list of triples `(x,
    y_\mathrm{min}, y_\mathrm{max})` corresponding to
    ``resolution`` equidistant `x` values.

    INPUT:

    - ``data`` -- a list (or iterable) of pairs of floats.

    - ``x_min`` -- a float, start of the interval.

    - ``x_max`` -- a float, end of the interval.

    - ``resolution`` -- a positive integer, the number of points
      in `x` direction.

    OUTPUT:

    A list of triples of floats.

    Each `(x, y)` is mapped to some
    `(x', y_\mathrm{min}, y_\mathrm{max})`
    such that `y_\mathrm{min}\le y\le y_\mathrm{max}` and
    `0\le x-x'< (x_\mathrm{max}-x_\mathrm{min})/\mathit{resolution}`.

    A list plot of the original list thus corresponds to plot of the
    vertical line segments defined by the output.

    EXAMPLES::

        sage: _reduce_resolution_(((i/10, i) for i in range(10)),
        ....:                    0, 1, 2)
        [(0.0, 0, 4), (0.5, 5, 9)]
        sage: _reduce_resolution_(((1 - i/10, i) for i in range(1, 11)),
        ....:                    0, 1, 2)
        [(0.0, 6, 10), (0.5, 1, 5)]
        sage: _reduce_resolution_(((0, 2), (0.2, 1), (0.4, 0),
        ....:                      (0.6, 1.5), (0.8, 3)), 0, 1, 2)
        [(0.0, 0, 2), (0.5, 1.50000000000000, 3)]
        sage: _reduce_resolution_([(0, 10)],
        ....:                    0, 1, 2)
        [(0.0, 10, 10)]

    TESTS::

        sage: _reduce_resolution_([(1, 10)],
        ....:                    0, 1, 2)
        Traceback (most recent call last):
        ...
        ValueError: x values must be >= x_min and < x_max.
        sage: _reduce_resolution_([(-1, 10)],
        ....:                    0, 1, 2)
        Traceback (most recent call last):
        ...
        ValueError: x values must be >= x_min and < x_max.
    """
    result = [None for _ in range(resolution)]
    f = (resolution) / (x_max - x_min)

    for (x, y) in data:
        if x < x_min and x > x_min - 1 / f:
            # Allow undershooting x_min by one pixel in order to be
            # more robust against numerical noise.
            x = x_min
        if x < x_min or x >= x_max:
            raise ValueError(
                "x values must be >= x_min and < x_max.")
        i = floor((x - x_min) * f)
        current = result[i]
        if current is None:
            result[i] = (y, y)
        else:
            result[i] = (min(y, current[0]), max(y, current[1]))

    return list(
        (x_min + float(i) / f, y[0], y[1])
        for i, y in enumerate(result)
        if y is not None)


def fluctuation_fourier(fourier_coefficients, start, end):
    resolution = len(fourier_coefficients)
    c0 = fourier_coefficients[0]
    fft = FastFourierTransform(resolution)
    for j, coefficient in enumerate(fourier_coefficients):
        fft[j] = coefficient

    fft.backward_transform()

    return [(float(i) / resolution, 2 * fft[i % resolution][0] - c0)
            for i in range(ceil(start * resolution),
                           ceil(end * resolution))]


def plot_fluctuation_asymptote(prefix, fourier_coefficients, empirical_values,
                               start, end, width=4, resolution=1200):
    r"""
    Provide `asymptote <http://asymptote.sourceforge.net>`_  files
    for the periodic fluctuation.

    INPUT:

    - ``prefix`` -- a string, filename for asymptote files. Three
      files are generated: a ``.asy``, an ``-empirical.dat`` and a
      ``-fourier.dat`` file; the latter two are read by asymptote
      when compiling the ``.asy`` file.

    - ``start`` -- a double, start of the plotting interval.

    - ``end`` -- a double, end of the plotting interval.

    - ``width`` -- (default: 4) a double giving
      the width in inches for the plotting area (excluding
      labels).

    - ``resolution`` -- (default: 1200) a positive integer, number
      of points per inch.

    OUTPUT:

    None.

    """
    template = r'''
import graph;
import size10;
defaultpen(fontsize(10pt)+fontcommand("\normalsize"));
locale("C");
size(0, 2inch, false);
unitsize(%ginch, 0);

file in = input("%s-empirical.dat").line();
real[][] a=in.dimension(0,0);
a=transpose(a);
real[] x=a[0];
real[] y_min=a[1];
real[] y_max=a[2];
int i;
for(i=0; i<x.length; ++i) {
  if (y_min[i] < y_max[i]) {
    draw((x[i],y_min[i])--(x[i],y_max[i]), blue);
  } else {
    draw((x[i],y_min[i]), blue);
  }
}

file in = input("%s-fourier.dat").line();
real[][] a=in.dimension(0,0);
a=transpose(a);
real[] x=a[0];
real[] y=a[1];
draw(graph(x,y), red+0.25bp);

xaxis(Bottom, RightTicks("$%%f$", Step=1, step=0.25), xmax=%g);
yaxis(Left,RightTicks(trailingzero));
'''

    empirical = reduce_resolution(
        empirical_values,
        start,
        end,
        int(resolution * width))

    fourier = fluctuation_fourier(fourier_coefficients,
                                  start,
                                  end)

    with open("%s-empirical.dat" % prefix, "w") as f:
        for x, y, z in empirical:
            f.write("%g %g %g\n" % (x, y, z))
    with open("%s-fourier.dat" % prefix, "w") as f:
        for x, y in fourier:
            f.write("%g %g\n" % (x, y))
    with open("%s.asy" % prefix, "w") as f:
        f.write(template % (float(width) / (end - start),
                            prefix, prefix, end + 0.001))


def arb_binomial(s, k):
    return prod(s - j for j in range(k)) / ZZ(k).factorial()


def vector_contains_zero(v):
    return all(0 in z for z in v)


class RegularPlotter(object):
    def __init__(self, q, matrices, left, right, R=None, CBF=None):
        self.q = ZZ(q)
        self.A = matrices
        if not len(matrices) == q:
            raise ValueError("There must be {} matrices".format(q))
        self.left = left
        self.right = right
        self.n = len(left)
        if not len(self.right) == self.n:
            raise ValueError("Right vector must have length {}".format(self.n))
        if not all(M.dimensions() == (self.n, self.n) for M in self.A):
            raise ValueError(
                "All matrices must have dimension {n} times {n}".format(
                    n=self.n))
        self.C = sum(self.A)

        self.CBF = CBF
        if not self.CBF:
            self.CBF = ComplexBallField()
        self.RBF = self.CBF.base()
        self.vector_space = VectorSpace(self.CBF, self.n)
        self.matrix_space = MatrixSpace(self.CBF, self.n, self.n)
        self.identity_matrix = self.matrix_space.identity_matrix()

        self.max_norm_A_r = self.RBF(max(M.norm(Infinity) for M in self.A))
        if R:
            self.R = R
        else:
            self.R = self.max_norm_A_r

        self.log_q_max_norm_A_r = self.RBF(
            log(self.max_norm_A_r, base=self.q))

        self.J, self.T_inverse = self.C.jordan_form(QQbar, transformation=True)
        self.T = self.T_inverse.inverse()
        assert self.T * self.C * self.T_inverse == self.J
        self.c_all = matrix(self.left) * self.T_inverse
        assert self.c_all * self.T == matrix(self.left)

        subdivisions = self.J.subdivisions()[0]
        self.T_inverse.subdivide([], subdivisions)
        self.T.subdivide(subdivisions, [])
        self.c_all.subdivide([], subdivisions)

        indices_large_eigenvalues = list(
            j for j in range(len(subdivisions) + 1)
            if abs(self.J.subdivision_entry(j, j, 0, 0)) > self.R)
        if any(self.J.subdivision(j, j).nrows() > 1
               for j in indices_large_eigenvalues):
            raise NotImplementedError("Multiple eigenvalues not supported")

        self.eigenvalue = list(
            self.CBF(self.J.subdivision_entry(j, j, 0, 0))
            for j in indices_large_eigenvalues)
        print("lambda", self.eigenvalue)
        self.w = list(
            self.vector_space(self.T.subdivision(j, 0).row(0))
            for j in indices_large_eigenvalues)
        assert all(
            vector_contains_zero(w * self.C - eigenvalue * w)
            for eigenvalue, w in zip(self.eigenvalue, self.w))
        self.c = list(
            self.CBF(self.c_all.subdivision_entry(0, j, 0, 0))
            for j in indices_large_eigenvalues)
        print("c", self.c)

        self.D = diagonal_matrix((
            ZZ(diagonal_element != 1)
            for diagonal_element in self.J.diagonal()))
        self.C_prime = self.T_inverse * self.D * self.J * self.T
        self.K = (
            self.T_inverse * self.D * self.T
            * (identity_matrix(self.n) - self.C_prime).inverse()
            * (identity_matrix(self.n) - self.A[0])
        )
        self.theta_1 = (
            self.T_inverse
            * (identity_matrix(self.n) - self.D)
            * self.T
            * (identity_matrix(self.n) - self.A[0])
        )

    @cached_method
    def f(self, n):
        if n == 0:
            return self.right
        m, r = ZZ(n).quo_rem(self.q)
        return self.A[r] * self.f(m)

    @cached_method
    def norm_A_r(self, r):
        return self.RBF(self.A[r].norm(Infinity))

    @cached_method
    def pole(self, j, ell):
        I = self.CBF(0, 1)
        if ell:
            return self.pole(j, 0) + self.CBF(
                2 * ell * self.CBF(pi) * I / log(self.q))
        return self.CBF(log(self.eigenvalue[j], base=self.q))

    def truncation_error_dirichlet_series(self, n_0, sigma):
        if sigma <= self.log_q_max_norm_A_r + 1:
            raise ValueError(
                "Re(s) must be larger than {}".format(
                    self.log_q_max_norm_A_r + 1
                ))
        return self.max_norm_A_r / (
            (n_0 - 1)**(sigma - self.log_q_max_norm_A_r - 1)
            * (sigma - self.log_q_max_norm_A_r - 1)
        )

    def truncation_error_functional_equation(self, n_0, s, j,  K):
        sigma = s.real()
        if sigma + K <= 0:
            raise ValueError("Re(s)+K must be positive")
        if sigma + K <= self.log_q_max_norm_A_r + 1:
            raise ValueError(
                "Re(s)+K must be larger than {}".format(
                    self.log_q_max_norm_A_r + 1
                ))
        error = self.q**(-sigma) * abs(arb_binomial(-s, K)) * sum(
            self.norm_A_r(r=r) * (r / self.q)**K
            for r in srange(self.q)
        ) * self.truncation_error_dirichlet_series(n_0, sigma + K)

        return error.abs()

    def error_vector(self, bound):
        error_RBF = self.RBF(rad=bound)
        error = self.CBF(error_RBF, error_RBF)
        return self.vector_space(self.n * [error])

    def G(self, n_0, j, ell, shift):
        r"""
        Compute
        ``\mathcal{G}_{n_0}(\log_q\lambda_j + \chi_{\ell} + \mathit{shift})``

        Complex balls have been avoided as arguments in order to have
        reliable caching.
        """
        K = 1
        s = self.pole(j=j, ell=ell) + shift
        result = sum(n**(-s) * self.f(n) for n in srange(n_0, self.q * n_0))
        while True:
            target_error = result.norm(Infinity) \
                * ZZ(2)**(-self.CBF.precision())
            current_error = self.truncation_error_functional_equation(
                n_0=n_0, s=s, j=j, K=K)
            if current_error < target_error:
                return result + self.error_vector(current_error.upper())
            result += self.q**(-s) * sum(
                self.A[r] * arb_binomial(-s, K) * (r / self.q)**K
                * self.F(n_0=n_0, j=j, ell=ell, shift=shift + K)
                for r in srange(self.q))
            K += 1

    @cached_method
    def F(self, n_0, j, ell, shift):
        s = self.pole(j=j, ell=ell) + shift
        return (self.identity_matrix - self.q**(-s) * self.C).solve_right(
            self.G(n_0=n_0, j=j, ell=ell, shift=shift))

    @cached_method
    def F_direct(self, n_0, n_1, j, ell, shift):
        s = self.pole(j=j, ell=ell) + shift
        result = sum(n**(-s) * self.f(n) for n in srange(n_0, n_1))
        error = self.truncation_error_dirichlet_series(n_0=n_1, sigma=s.real())
        return result + self.error_vector(error.upper())

    def residue_F(self, n_0, j, ell):
        return self.w[j] * self.G(n_0=n_0, j=j, ell=ell, shift=0) / log(self.q)

    @cached_method
    def fourier_coefficient(self, n_0, j, ell):
        return self.residue_F(n_0=n_0, j=j, ell=ell) / self.pole(j=j, ell=ell)

    @cached_method
    def summatory_function(self, n):
        if n:
            return self.f(n - 1) + self.summatory_function(n - 1)
        else:
            return 0

    def plot(self, j, start, end, prefix, number_fourier_coefficients,
             n_0=ZZ(1024), restrict_to_eigenspace=True,
             subtract_constant=False):
        if restrict_to_eigenspace:
            left = self.c[j] * self.w[j]
        else:
            left = self.left

        if subtract_constant:
            constant = left * self.K * self.right
        else:
            constant = 0

        start_time = time.time()
        print("Filling cache with empirical values")
        # fill cache with lower values of summatory function
        # in order to avoid stack overflow
        list(self.summatory_function(n) for n in srange(self.q ** start))
        duration = time.time() - start_time
        print("Done filling cache with empirical values ({} seconds)".format(
            duration))
        log_lambda = self.pole(j=j, ell=0)
        empirical_values = list(
            (
                RR(log(n, base=self.q)),
                RR(
                    (left * self.summatory_function(n) - constant)
                    / (n**log_lambda).real()
                )
            )
            for n in srange(self.q ** start, self.q ** end))
        fourier_coefficients = list(
            CC(self.c[j] * self.fourier_coefficient(n_0=n_0, j=j, ell=ell))
            for ell in srange(number_fourier_coefficients))
        plot_fluctuation_asymptote(
            prefix=prefix,
            fourier_coefficients=fourier_coefficients,
            empirical_values=empirical_values,
            start=start,
            end=end)
