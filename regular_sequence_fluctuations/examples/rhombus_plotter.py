# ****************************************************************************
#       Copyright (C) 2019 Clemens Heuberger <clemens.heuberger@aau.at>
#                     2019 Daniel Krenn <dev@danielkrenn.at>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/
# ****************************************************************************

import sys

from sage.all import matrix, vector, ComplexBallField

from regular_sequence_fluctuations.plot_fluctuations import RegularPlotter


class RhombusPlotter(RegularPlotter):
    rhombus_matrix_0 = matrix([
            [1, 0, 0, 1, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 1, 0],
            [2, 0, 0, 0, 0],
            [0, 0, 2, 0, 0]])
    rhombus_matrix_1 = matrix([
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 1],
            [1, 0, 0, 0, 1],
            [0, 0, 2, 0, 0],
            [0, 2, 0, 0, 0]
        ])
    rhombus_left_vector = vector([1, 0, 0, 0, 0])
    rhombus_right_vector = vector([0, 1, 1, 0, 2])

    def rhombus_matrices(self):
        return [self.rhombus_matrix_0, self.rhombus_matrix_1]

    def __init__(self, **kwargs):
        super(RhombusPlotter, self).__init__(
              q=2,
              matrices=self.rhombus_matrices(),
              left=self.rhombus_left_vector,
              right=self.rhombus_right_vector,
              R=2,
              **kwargs
             )


if __name__ == "__main__":
    sys.setrecursionlimit(2048)
    plotter = RhombusPlotter(CBF=ComplexBallField(128))
    for ell in range(11):
        print(
            ell,
            plotter.c[0] * plotter.CBF(
                plotter.fourier_coefficient(1024, 0, ell))
            )
    plotter = RhombusPlotter()
    plotter.plot(
        0, 9, 13, "pascal_rhombus_plot", 2000,
        restrict_to_eigenspace=False)
