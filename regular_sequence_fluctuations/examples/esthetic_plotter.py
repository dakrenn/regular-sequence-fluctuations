# ****************************************************************************
#       Copyright (C) 2019 Clemens Heuberger <clemens.heuberger@aau.at>
#                     2019 Daniel Krenn <dev@danielkrenn.at>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/
# ****************************************************************************

import sys

from sage.all import matrix, vector, CIF

from regular_sequence_fluctuations.plot_fluctuations import RegularPlotter


class EstheticPlotter(RegularPlotter):
    def esthetic_matrix(self, q, digit):
        def esthetic_matrix_entry(q, digit, i, j):
            if j != digit:
                return 0
            if i == q:
                return 1
            if abs(i-digit) == 1:
                return 1
            return 0

        return matrix(
            q+1, q+1, lambda i, j: esthetic_matrix_entry(q, digit, i, j))

    def esthetic_matrices(self, q):
        return [self.esthetic_matrix(q, digit) for digit in range(q)]

    def __init__(self, q):
        super(EstheticPlotter, self).__init__(
              q=q,
              matrices=self.esthetic_matrices(q),
             left=vector(q * [0] + [1]),
             right=vector([0] + q * [1])
             )


if __name__ == "__main__":
    sys.setrecursionlimit(2048)
    plotter = EstheticPlotter(4)
    plotter.plot(1, 9, 12, "esthetic", 2000,
                 restrict_to_eigenspace=False)
    for ell in range(11):
        print(
            ell,
            CIF(plotter.c[1] * plotter.CBF(
                plotter.fourier_coefficient(1024, 1, ell))
                ),
            sep='&')
